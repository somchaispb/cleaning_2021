-- определения узких мест
/*Узкие места могут быть при использовании селективного поля для частого поиска.
-- Также, работа может замедляться, если не будет индексов на полях, предназначенных для джойна таблицы с другой таблицой по первичному ключу. Необходимо создать полнотекстовый индекс на селективные поля с высокой кардинальностью.
*/

--   Создать индекс к какой-либо из таблиц вашей БД
/* Часть индексов в БД уже была создана при создании таблиц внутри скрипта DML:*/
CREATE INDEX fkIdx ON workshop.orders (external_status_id);
CREATE UNIQUE INDEX idx_roles_name_unique ON users.roles (name);
CREATE INDEX idx_customers_lname ON workshop.customers(lname);

--     Прислать текстом результат команды explain, в которой используется данный индекс
explain analyse
select lname
FROM workshop.customers
where lname = 'Яровая';
/*
Index Only Scan using idx_customers_lname on customers  (cost=0.14..8.16 rows=1 width=318) (actual time=2.697..2.700 rows=1 loops=1)
  Index Cond: (lname = 'Яровая'::text)
  Heap Fetches: 1
Planning Time: 0.050 ms
Execution Time: 2.708 ms
 */

--     Реализовать индекс для полнотекстового поиска
CREATE INDEX customers_lname_trg_idx ON workshop.customers USING gin(to_tsvector('russian', lname));

--     Реализовать индекс на часть таблицы или индекс на поле с функцией
CREATE INDEX customers_mobile_part_idx ON workshop.customers (mobile) WHERE mobile IS NOT NULL;
CREATE INDEX customers_fname_lname_lower_idx ON workshop.customers (lower(lname), lower(fname));

--     Создать индекс на несколько полей
CREATE INDEX users_email_username_idx ON users.users (email, username);

--     Написать комментарии к каждому из индексов
/*
    Все индексы были созданы для ускорения доступа к полю, а также для быстрого полнотекстового полиска. Для неизменяющихся таблиц, в будущем необходимо создать materalized view с векторизированными селективными полями. Обновление представления необходимо выполнять по расписанию ночью.
 */
--     Описать что и как делали и с какими проблемами столкнулись
/*
 Были созданы индексы удовлетворящие требованиям задания.
 Был создан уникальный индекс, BTREE индекс, полнотекстовый GIN индекс с векторизацией, и композитный индекс на несколько полей. Трудностей не возникло, потому что схема БД довольно простая.
 Проблемы бы возникли при создании индекса на поле JSONB, однако такого типа поля в данной БД нет.
 */