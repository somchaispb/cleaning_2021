insert into workshop.customers (fname, lname, mobile, address, discount)
values
('Егор', 'Смирнов', '+79877656543', 'Спб. ул. Белы Куна, д.23, к2, кв 45', 0),
('Екатерина', 'Бобкова', '+79113456543', 'Спб. ул. Ленсовета, д.1, кв 11', 0),
('Павел', 'Волков', '+79516543466', 'Спб. Московский пр., д.78, кв 90', 0),
('Ольга', 'Яровая', '+78126784411', 'Спб. Колокольный пер., д.65, к1, оф. 1', 0.5);

insert into workshop.internal_statuses (id, name)
values
(1, 'RECONCILIATION '),
(2, 'IN_PROCESS'),
(3, 'PACKING'),
(4, 'READY_TO_ISSUE'),
(5, 'ISSUED');

insert into workshop.external_statuses (id, name)
VALUES
(1, 'NEW'),
(2, 'IN_PROCESS'),
(3, 'READY_TO_DELIVER'),
(4, 'ON_THE_WAY'),
(5, 'COMPLETE');

insert into delivery.delivery_statuses (id, name)
VALUES
(1, 'ON_THE_WAY_TO_CUSTOMER'),
(2, 'ON_THE_WAY_FROM_CUSTOMER'),
(3, 'PICKED_UP'),
(4, 'DELIVERED');

INSERT INTO workshop.categories (id, name)
VALUES
(1, 'химчистка'),
(2, 'аквачистка'),
(3, 'сушка');

INSERT INTO workshop.extra_attributes (id, name, price, description)
VALUES
(1, 'крупное загрязнение', 500, 'крупное загрязнение изделия'),
(2, 'кровь', 1000, 'биологический материал'),
(3, 'бережная стирка', 400, 'деликатное издение'),
(4, 'пластиковые пуговицы', 800, 'отшив и пришивание пуговиц перед принятием в работу'),
(5, 'детское белье', 300, 'мягкая химия'),
(6, 'тонкая ткань', 800, 'риски на разрыв изделия'),
(7, 'стандартная стирка', 0, 'стандартное изделие');

INSERT INTO workshop.services (name, category_id, extra_atribute_id, price)
VALUES
('текстиль', 1, 7, 1500),
('кожа', 1, 7, 1000),
('мех', 1, 7, 2500),
('домашний текстиль', 2, 7, 800);

INSERT INTO workshop.urgency_types (id, name, coefficient)
VALUES
(1, 'нормальный', 0),
(2, 'срочный', 0.4);

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

INSERT INTO workshop.orders (hash, external_status_id, customer_id, total_price, description)
VALUES
(uuid_generate_v4(), 1, 1, 100, 'частник'),
(uuid_generate_v4(), 1, 2, 200, 'частник'),
(uuid_generate_v4(), 1, 3, 300, 'частник'),
(uuid_generate_v4(), 1, 4, 500, 'юр лицо');

INSERT INTO workshop.payment_types (id, name)
VALUES
(1, 'CASH'),
(2, 'ONLINE'),
(3, 'BANK_TRANSFER');

INSERT INTO workshop.payment_statuses (id, name)
VALUES
(1, 'NOT_PAID'),
(2, 'WAITING_FOR_PAYMENT'),
(3, 'PAID');

INSERT INTO workshop.payments (payment_type, order_id, payment_status_id)
VALUES
(2, 1, 1),
(2, 2, 2),
(1, 3, 1),
(3, 4, 3);

INSERT INTO workshop.order_service (order_id, service_id, urgency_type_id, internal_status_id, quantity, price, description)
VALUES
(1, 2, 1, 1, 2, 500, 'кожаное пальто'),
(2, 3, 1, 1, 2, 800, 'шуба'),
(3, 1, 2, 2, 2, 1000, 'брюки'),
(4, 4, 1, 2, 2, 5000, null);


-- Напишите запрос по своей базе с регулярным выражением, добавьте пояснение, что вы хотите найти.
-- Ищем клиента с фамилией где есть вхождение яров(любая буква)я
SELECT * from workshop.customers
where lname ILIKE '%яров_я%';

-- Ищем все вхождения чистка в строке
SELECT * from workshop.categories
where name ILIKE '%чистка%';

-- Напишите запрос по своей базе с использованием LEFT JOIN и INNER JOIN, как порядок соединений в FROM влияет на результат? Почему?
-- выбираем ВСЕХ клиентов с заказами и услугами даже, если заказов у клиента нет (NULL) из таблиц с помощью LEFT JOIN
SELECT o.id, os.price, concat(c.lname, ' ', c.fname), s.name
from workshop.orders o
         LEFT JOIN workshop.order_service os on o.id = os.order_id
         LEFT JOIN workshop.services s on s.id = os.service_id
         LEFT JOIN workshop.customers c on c.id = o.customer_id;

-- Выбираем клиентов только с заказами с помощью INNER JOIN. Клиенты без заказов не отображаются
SELECT o.id, os.price, concat(c.lname, ' ', c.fname), s.name
from workshop.orders o
         INNER JOIN workshop.order_service os on o.id = os.order_id
         LEFT JOIN workshop.services s on s.id = os.service_id
         INNER JOIN workshop.customers c on c.id = o.customer_id;

-- Напишите запрос на добавление данных с выводом информации о добавленных строках.
insert into workshop.customers (fname, lname, mobile, address, discount)
values
('Дмитрий', 'Ярунов', '+79877776544', 'Спб. Невский пр. д.90, кв.120', 0),
('Том', 'Кукуруз', '+8900786532', 'Спб. ул. Итальянская д.6, кв.9', 0.1)
    RETURNING id, lname;

-- Напишите запрос с обновлением данные используя UPDATE FROM. В презентации, видимо, указан неправильный синтаксис.
-- UPDATE alias1 SET column1 = alias2.value1 FROM table1 as alias1 JOIN table2 as alias2 ON table1.id = table2.id WHERE alias1.column2 = value2 Это не работает.
UPDATE workshop.customers c
set mobile = c2.hash
    FROM workshop.orders c2
where c.id = 2;

-- Напишите запрос для удаления данных с оператором DELETE используя join с другой таблицей с помощью using.
DELETE FROM workshop.order_service os
    USING workshop.orders o
where os.order_id = o.id
  AND o.total_price <= 100
    RETURNING *;

-- Приведите пример использования утилиты COPY (по желанию)
COPY workshop.customers TO STDOUT (DELIMITER '|');

COPY (SELECT * FROM workshop.customers c WHERE c.lname = 'Волков') TO '/home/denis/copy_test.copy' (DELIMITER '|');


