-- Научиться джойнить таблицы и использовать условия в SQL выборке
--     Напишите запрос по своей базе с inner join
    select c.id, c.fname, c.lname
    from customers c
    INNER JOIN orders o on c.id = o.customer_id
    group by c.id;

--     Напишите запрос по своей базе с left join
select c.id, c.fname, c.lname
from customers c
LEFT JOIN orders o on c.id = o.customer_id
group by c.id;

select c.id, c.fname, c.lname, o.external_status_id, os.description
from customers c
LEFT JOIN orders o on c.id = o.customer_id
LEFT JOIN order_service os on o.id = os.order_id;

--     Напишите 5 запросов с WHERE с использованием разных операторов, опишите для чего вам в проекте нужна такая выборка данных

-- Выбрать всех клиентов у которых сумма цены заказов больше 100.
select c.id, c.fname, c.lname, o.external_status_id, os.description
from customers c
         LEFT JOIN orders o on c.id = o.customer_id
         LEFT JOIN order_service os on o.id = os.order_id
WHERE o.total_price > 100;

-- Выбрать услуги по химчистке
select *
from services
where name = 'химчистка';

-- Выбрать услуги по химчистке
select *
from services
where name = 'химчистка';

-- Выбрать все заказы в определенном статусе
select *
from orders
         left join external_statuses es on orders.external_status_id = es.id
where es.id = 1
   OR es.id BETWEEN 4 AND 5;

-- Выбрать все доставки в статусах кроме 1
SELECT *
from delivery.deliveries
         left join delivery.delivery_statuses ds on ds.id = deliveries.delivery_status_id
where delivery_status_id <> 1;


