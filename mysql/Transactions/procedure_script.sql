-- Заполнение своего проекта данными
--     Описать пример транзакции из своего проекта с изменением данных в нескольких таблицах. Реализовать в виде хранимой процедуры.

CREATE PROCEDURE test_transaction(IN order_id INT, IN status INT, OUT result INT)
BEGIN
select * from workshop.orders
                  join external_statuses es on orders.external_status_id = es.id
    for update;
UPDATE workshop.orders
set external_status_id = status
where workshop.orders.id = order_id;
-- More tables to update, can not find the case
select id INTO result from workshop.orders  where id = @order_id;
commit ;
end;

call test_transaction(1, 2, @result);

-- Загрузить данные из приложенных в материалах csv. Реализовать следующими путями:
--     LOAD DATA
-- Создаем таблицу под CSV файл
CREATE TABLE IF NOT EXISTS apparels(
    id BIGINT PRIMARY KEY ,
    handle varchar(255),
    title varchar(255) NULL,
    body text NULL,
    type varchar(255) NULL,
    tags varchar(255),
    is_published bool null,
    option1_name varchar(255) null,
    option1_value varchar(255) null,
    option2_name varchar(255) null,
    option2_value varchar(255) null,
    option3_name varchar(255) null,
    option3_value varchar(255) null,
    sku varchar(255) null,
    variant_grams int null,
    variant_inventory_tracker varchar(100),
    variant_qty int null,
    variant_policy varchar(255) null,
    variant_service varchar(255) null,
    variant_price float null,
    variant_compare_at_price float null,
    requires_shipping bool null,
    is_taxable bool null,
    barcode varchar(255) null,
    image_src text null,
    image_alt_text varchar(300) null,
    is_gift_card bool null,
    seo_title varchar(255) null,
    seo_description text null
);

SHOW VARIABLES LIKE 'local_infile';
SET GLOBAL local_infile = 1;

-- Загружаем CSV
LOAD DATA LOCAL INFILE '/home/denis/Documents/SQL/cleaning_2021/mysql/Transactions/apparels.csv'
INTO TABLE apparels
fields terminated by ','
ENCLOSED BY '"'
lines terminated by '\n'
IGNORE 1 ROWS
(
handle,
title,
body,
type,
tags,
is_published,
option1_name,
option1_value,
option2_name,
option2_value,
option3_name,
option3_value,
sku,
variant_grams,
variant_inventory_tracker,
variant_qty,
variant_policy,
variant_service,
variant_price,
variant_compare_at_price,
requires_shipping,
is_taxable,
barcode,
image_src,
image_alt_text,
is_gift_card,
seo_title,
seo_description);


--     mysqlimport
mysqlimport -h 127.0.0.1 -P3309 -uroot -proot cleaning_2021 --ignore-lines=1 --lines-terminated-by="\n" --fields-terminated-by="," --fields-enclosed-by="\""  -c handle,title,body,type,tags,is_published,option1_name,option1_value,option2_name,option2_value,option3_name,option3_value,sku,variant_grams,variant_inventory_tracker,variant_qty,variant_policy,variant_service,variant_price,variant_compare_at_price,requires_shipping,is_taxable,barcode,image_src,image_alt_text,is_gift_card,seo_title,seo_description "/home/denis/Documents/SQL/cleaning_2021/mysql/Transactions/apparels.csv"

-- mysqlimport: Error: 13, Can't get stat of '/home/denis/Documents/SQL/cleaning_2021/mysql/Transactions/apparels.csv' (OS errno 2 - No such file or directory), when using table: apparels

