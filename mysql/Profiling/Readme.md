# Explain

```sql
desc select
min(s.salary) as min_salary,
max(s.salary) as max_salary,
d.dept_no
from salaries s
join employees e on e.emp_no = s.emp_no
join dept_emp de on e.emp_no = de.emp_no
join departments d on d.dept_no = de.dept_no
group by d.dept_no with rollup
limit 500;
```
```sql
1,SIMPLE,e,,index,PRIMARY,PRIMARY,4,,299556,100,Using index; Using temporary; Using filesort
1,SIMPLE,de,,ref,"PRIMARY,dept_no",PRIMARY,4,employees.e.emp_no,1,100,Using index
1,SIMPLE,d,,eq_ref,"PRIMARY,dept_name",PRIMARY,16,employees.de.dept_no,1,100,Using index
1,SIMPLE,s,,ref,PRIMARY,PRIMARY,4,employees.e.emp_no,9,100,
```
# format json
```sql
desc FORMAT=JSON select
     min(s.salary) as min_salary,
     max(s.salary) as max_salary,
     d.dept_no
 from salaries s
          join employees e on e.emp_no = s.emp_no
          join dept_emp de on e.emp_no = de.emp_no
          join departments d on d.dept_no = de.dept_no
          left join dept_manager dm on e.emp_no = dm.emp_no
 group by d.dept_no with rollup
 limit 500;
```
# format tree
```sql
desc FORMAT=TREE select
     min(s.salary) as min_salary,
     max(s.salary) as max_salary,
     d.dept_no
 from salaries s
          join employees e on e.emp_no = s.emp_no
          join dept_emp de on e.emp_no = de.emp_no
          join departments d on d.dept_no = de.dept_no
          left join dept_manager dm on e.emp_no = dm.emp_no
 group by d.dept_no with rollup
 limit 500;
```
- не работает. [HY000][1791] Unknown EXPLAIN format name: 'TREE'

В демо базе созданы нужные индексы. 

```sql
EXPLAIN ANALYZE  select
    min(s.salary) as min_salary,
max(s.salary) as max_salary,
       d.dept_no
from salaries s
         join employees e on e.emp_no = s.emp_no
         join dept_emp de on e.emp_no = de.emp_no
         join departments d on d.dept_no = de.dept_no
        left join dept_manager dm on e.emp_no = dm.emp_no
group by d.dept_no with rollup
limit 500;
```
- не работает. [42000][1064] You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'ANALYZE'

```sql
SET GLOBAL slow_query_log = 1;
SET GLOBAL slow_launch_time = 1;
```

```sql
SHOW variables like 'slow%';

slow_launch_time,1
slow_query_log,ON
slow_query_log_file,/var/lib/mysql/127fc87a33a3-slow.log
```


