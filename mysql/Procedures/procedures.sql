
create user 'client'@'localhost';
create user 'manager'@'localhost';

create procedure get_employees(IN d CHAR(4), e INTEGER(11), ln VARCHAR(16))
BEGIN
SELECT *
FROM employees e
         left join dept_emp de on e.emp_no = de.emp_no
         left join departments d on de.dept_no = d.dept_no
where de.dept_no = d
   OR de.emp_no = e
   OR e.last_name = ln;
end;

GRANT EXECUTE ON PROCEDURE employees.get_employees TO 'client'@'localhost';

call get_employees('d009', 10004, '');

create procedure get_salaries(IN emp_id INTEGER(11), from_d INTEGER(4), to_d INTEGER(4), field VARCHAR(10))
BEGIN
SELECT SUM(salary)
from salaries
where from_d > YEAR(from_date)
  AND to_d < YEAR(to_date)
  AND emp_no = emp_id
group by field;
end;

GRANT EXECUTE ON PROCEDURE employees.get_salaries TO 'manager'@'localhost';

call get_salaries(1011, 1980, 2000, 'emp_no');
