1. проанализировать типы данных в своем проекте, изменить при необходимости. В README указать что на что поменялось и почему.


- После анализа структуры данных, было принято решение поменять тип данных для первичных ключей с SERIAL на BIGINT PRIMARY KEY.
- Также было принято решение добавить поле params в таблицу workshop.customers в целях обучения работы с JSON

2. добавить тип JSON в структуру. Проанализировать какие данные могли бы там хранится. привести примеры SQL для добавления записей и выборки.

- Тип JSON добавлен в таблицу workshop.customers поле params
- Пример SQL для вставки:
```sql
insert into workshop.customers(fname, lname, mobile, address, discount, hash, params)
values(
    'Vasiliy', 'toporov', '+7999765456', 'some address', 0, null, JSON_OBJECT('uiSettings', JSON_OBJECT('layout', 'block'), 'personalData', JSON_OBJECT('preferences', JSON_OBJECT('favorites', JSON_OBJECT('node', 'test node', 'order', 1))))
      );
```
- Пример SQL для поиска в JSON
```sql
select params,
       json_extract(params, "$.*"),
       params ->> "$.*.preferences.favorites.order" as t
from workshop.customers
where json_contains(params->'$.*.preferences.favorites.order', '[1]')
```

Результатом домашней работы стал навык добавления JSON объектов в поле с последующим поиском и выборкой из поля JSON

