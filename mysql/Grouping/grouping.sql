-- предоставить следующий результат группировки с ипользованием CASE, HAVING, ROLLUP, GROUPING() : для магазина к предыдущему списку продуктов добавить максимальную и минимальную цену и кол-во предложений также сделать выборку показывающую самый дорогой и самый дешевый товар в каждой категории сделать rollup с количеством товаров по категориям
-- DB = https://github.com/datacharmer/test_db

select
    YEAR(from_date) as from_date_year,
    YEAR(to_date) as to_date_year,
    SUM(salary) as total
from salaries
group by YEAR(from_date), YEAR(to_date) with ROLLUP ;

select
    d.dept_no,
    d.dept_name,
    SUM(salary) as total
from salaries s
         join employees e on e.emp_no = s.emp_no
         join dept_emp de on e.emp_no = de.emp_no
         join departments d on d.dept_no = de.dept_no
group by d.dept_no, d.dept_name with ROLLUP
limit 500;

select
    min(s.salary) as min_salary,
    max(s.salary) as max_salary,
    d.dept_no
from salaries s
         join employees e on e.emp_no = s.emp_no
         join dept_emp de on e.emp_no = de.emp_no
         join departments d on d.dept_no = de.dept_no
group by d.dept_no
limit 500;









