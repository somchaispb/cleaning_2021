-- Задача - сделать полнотекстовый индекс, который ищет по свойствам, названию товара и описанию в README представить запрос для тестирования


-- анализируем свой проект - добавляем или обновляем индексы в README пропишите какие индексы были изменены или добавлены explain и результаты выборки без индекса и с индексом
DROP INDEX idx_customers_mobile_unique ON workshop.customers;
EXPLAIN select * from workshop.customers where mobile = '12234';
-- 1,SIMPLE,customers,,ALL,,,,,1,100,Using where
ALTER TABLE workshop.customers ADD UNIQUE INDEX idx_customers_mobile_unique(mobile);
EXPLAIN select * from workshop.customers where mobile = '12345';
-- 1,SIMPLE,customers,,const,idx_customers_mobile_unique,idx_customers_mobile_unique,202,const,1,100,


