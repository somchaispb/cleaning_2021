# Start mongo in docker

```
mongo -u root
```

## inserting data

```
db.inventory.insertMany([
   { item: "journal", qty: 25, size: { h: 14, w: 21, uom: "cm" }, status: "A" },
   { item: "notebook", qty: 50, size: { h: 8.5, w: 11, uom: "in" }, status: "A" },
   { item: "paper", qty: 100, size: { h: 8.5, w: 11, uom: "in" }, status: "D" },
   { item: "planner", qty: 75, size: { h: 22.85, w: 30, uom: "cm" }, status: "D" },
   { item: "postcard", qty: 45, size: { h: 10, w: 15.25, uom: "cm" }, status: "A" }
]);
```

## selecting data
```
db.inventory.find( {} )
```

## updating data
```
db.inventory.update({_id: ObjectId("61054621763581151c62107d") }, {$set: {item: "mew item"}})
```

## Creating index
```
 db.inventory.createIndex({"item": 1, "status": -1})
```
