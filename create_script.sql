create role admin;

alter role admin login createdb;

create schema workshop;
create schema delivery;
create schema users;

set search_path TO workshop, users, delivery;

create tablespace cleaning owner admin location '/home/denis/Documents/SQL/postgres';

create database cleaning2021 tablespace = cleaning;

CREATE TABLE IF NOT EXISTS workshop.customers
(
    "id"     BIGSERIAL,
    fname    varchar(150) NOT NULL,
    lname    varchar(150) NOT NULL,
    mobile   varchar(50) NOT NULL,
    address  text NOT NULL,
    discount double precision NULL,
    hash     text NULL,
    CONSTRAINT PK_customers PRIMARY KEY ( "id" ),
    CONSTRAINT idx_customers_hash_unique UNIQUE ( hash ),
    CONSTRAINT idx_customers_mobile_unique UNIQUE ( mobile )
    );

CREATE INDEX idx_customers_lname ON workshop.customers
    (
     lname
        );

CREATE TABLE IF NOT EXISTS workshop.external_statuses
(
    "id"   BIGSERIAL,
    name varchar(50) NOT NULL,
    CONSTRAINT PK_statuses PRIMARY KEY ( "id" )
    );

CREATE TABLE IF NOT EXISTS workshop.payment_types
(
    "id"   BIGSERIAL,
    name varchar(50) NOT NULL,
    CONSTRAINT PK_payment_types PRIMARY KEY ( "id" )
    );

CREATE TABLE IF NOT EXISTS workshop.orders
(
    "id"                 BIGSERIAL,
    hash               text NOT NULL,
    external_status_id bigint NOT NULL,
    customer_id        bigint NOT NULL,
    total_price        double precision NULL,
    description        text NULL,
    CONSTRAINT PK_orders PRIMARY KEY ( "id" ),
    CONSTRAINT fk_external_status FOREIGN KEY ( external_status_id ) REFERENCES workshop.external_statuses ( "id" ),
    CONSTRAINT FK_81 FOREIGN KEY ( customer_id ) REFERENCES workshop.customers ( "id" )
    );

CREATE INDEX fkIdx_51 ON workshop.orders
    (
     external_status_id
        );

CREATE INDEX fkIdx_82 ON workshop.orders
    (
     customer_id
        );

CREATE TABLE IF NOT EXISTS workshop.payment_statuses
(
    "id"   BIGSERIAL,
    name varchar(50) NOT NULL,
    CONSTRAINT PK_payment_statuses PRIMARY KEY ( "id" )
    );


CREATE TABLE IF NOT EXISTS workshop.payments
(
    "id"                BIGSERIAL,
    payment_type      bigint NOT NULL,
    order_id          bigint NOT NULL,
    payment_status_id bigint NOT NULL,
    CONSTRAINT PK_payments PRIMARY KEY ( "id" ),
    CONSTRAINT fk_payment_type FOREIGN KEY ( payment_type ) REFERENCES workshop.payment_types ( "id" ),
    CONSTRAINT FK_63 FOREIGN KEY ( order_id ) REFERENCES workshop.orders ( "id" ),
    CONSTRAINT fk_payment_status FOREIGN KEY ( payment_status_id ) REFERENCES workshop.payment_statuses ( "id" )
    );

CREATE INDEX fkIdx_61 ON workshop.payments
    (
     payment_type
        );

CREATE INDEX fkIdx_64 ON workshop.payments
    (
     order_id
        );

CREATE INDEX fkIdx_71 ON workshop.payments
    (
     payment_status_id
        );



CREATE TABLE IF NOT EXISTS workshop.internal_statuses
(
    "id"   BIGSERIAL,
    name varchar(50) NOT NULL,
    CONSTRAINT PK_internal_statuses PRIMARY KEY ( "id" )
    );

CREATE TABLE IF NOT EXISTS workshop.extra_attributes
(
    "id"          BIGSERIAL,
    name        varchar(255) NOT NULL,
    price       double precision NULL,
    description text NULL,
    CONSTRAINT PK_extra PRIMARY KEY ( "id" ),
    CONSTRAINT idx_extra_attributes_name_unique UNIQUE ( name )
    );

CREATE TABLE IF NOT EXISTS workshop.categories
(
    "id"   BIGSERIAL,
    name varchar(100) NOT NULL,
    CONSTRAINT PK_categories PRIMARY KEY ( "id" ),
    CONSTRAINT idx_categories_name_unique UNIQUE ( name )
    );


CREATE TABLE IF NOT EXISTS workshop.services
(
    "id"                BIGSERIAL,
    name              varchar(255) NOT NULL,
    category_id       bigint NOT NULL,
    extra_atribute_id bigint NOT NULL,
    price             double precision NULL,
    CONSTRAINT PK_services PRIMARY KEY ( "id" ),
    CONSTRAINT FK_157 FOREIGN KEY ( extra_atribute_id ) REFERENCES workshop.extra_attributes ( "id" ),
    CONSTRAINT fk_categories FOREIGN KEY ( category_id ) REFERENCES workshop.categories ( "id" )
    );

CREATE INDEX fkIdx_158 ON workshop.services
    (
     extra_atribute_id
        );

CREATE INDEX fkIdx_37 ON workshop.services
    (
     category_id
        );

CREATE INDEX idx_services_name ON workshop.services
    (
     name
        );

CREATE TABLE IF NOT EXISTS workshop.urgency_types
(
    "id"          BIGSERIAL,
    name        varchar(50) NOT NULL,
    coefficient double precision NOT NULL,
    CONSTRAINT PK_urgency_types PRIMARY KEY ( "id" )
    );

CREATE TABLE IF NOT EXISTS workshop.order_service
(
    order_id           BIGSERIAL,
    service_id         bigint NOT NULL,
    urgency_type_id    bigint NOT NULL,
    internal_status_id bigint NOT NULL,
    quantity           int NOT NULL,
    price              double precision NULL,
    description        text NULL,
    CONSTRAINT idx_order_service_order_id_service_id_unique UNIQUE ( order_id, service_id ),
    CONSTRAINT fk_orders FOREIGN KEY ( order_id ) REFERENCES workshop.orders ( "id" ),
    CONSTRAINT fk_services FOREIGN KEY ( service_id ) REFERENCES workshop.services ( "id" ),
    CONSTRAINT FK_39 FOREIGN KEY ( urgency_type_id ) REFERENCES workshop.urgency_types ( "id" ),
    CONSTRAINT fk_internal_status FOREIGN KEY ( internal_status_id ) REFERENCES workshop.internal_statuses ( "id" )
    );

CREATE INDEX fkIdx_31 ON workshop.order_service
    (
     order_id
        );

CREATE INDEX fkIdx_34 ON workshop.order_service
    (
     service_id
        );

CREATE INDEX fkIdx_40 ON workshop.order_service
    (
     urgency_type_id
        );

CREATE INDEX fkIdx_79 ON workshop.order_service
    (
     internal_status_id
        );

CREATE TABLE IF NOT EXISTS delivery.delivery_statuses
(
    "id"   BIGSERIAL,
    name varchar(50) NOT NULL,
    CONSTRAINT PK_delivery_statuses PRIMARY KEY ( "id" )
    );

CREATE TABLE IF NOT EXISTS users.users
(
    "id"       BIGSERIAL,
    username varchar(150) NOT NULL,
    password varchar(255) NOT NULL,
    email    varchar(255) NOT NULL,
    CONSTRAINT PK_users PRIMARY KEY ( "id" ),
    CONSTRAINT idx_users_email_unique UNIQUE ( email )
    );

CREATE TABLE IF NOT EXISTS users.employee_types
(
    "id"   BIGSERIAL,
    name varchar(50) NOT NULL,
    CONSTRAINT PK_employee_type PRIMARY KEY ( "id" )
    );

CREATE TABLE IF NOT EXISTS users.employees
(
    "id"               BIGSERIAL,
    fname            varchar(150) NOT NULL,
    lname            varchar(150) NOT NULL,
    mobile           varchar(50) NOT NULL,
    ssn              integer NULL,
    employee_type_id bigint NOT NULL,
    user_id          bigint NULL,
    CONSTRAINT PK_employees PRIMARY KEY ( "id" ),
    CONSTRAINT idx_employees_mobile_unique UNIQUE ( mobile ),
    CONSTRAINT fk_employee_types FOREIGN KEY ( employee_type_id ) REFERENCES users.employee_types ( "id" ),
    CONSTRAINT fk_users FOREIGN KEY ( user_id ) REFERENCES users.users ( "id" )
    );

CREATE INDEX fkIdx_104 ON users.employees
    (
     employee_type_id
        );

CREATE INDEX fkIdx_145 ON users.employees
    (
     user_id
        );

CREATE INDEX idx_employees_lastname ON users.employees
    (
     lname
        );

CREATE TABLE IF NOT EXISTS delivery.deliveries
(
    "id"                 BIGSERIAL,
    order_id           bigint NOT NULL,
    employee_id        bigint NOT NULL,
    delivery_status_id bigint NOT NULL,
    price              double precision NULL,
    cost_price         double precision NULL,
    description        text NULL,
    CONSTRAINT PK_deliveries PRIMARY KEY ( "id" ),
    CONSTRAINT fk_employees FOREIGN KEY ( employee_id ) REFERENCES users.employees ( "id" ),
    CONSTRAINT fk_delivery_statuses FOREIGN KEY ( delivery_status_id ) REFERENCES delivery.delivery_statuses ( "id" )
    );

CREATE INDEX fkIdx_112 ON delivery.deliveries
    (
     employee_id
        );

CREATE INDEX fkIdx_152 ON delivery.deliveries
    (
     delivery_status_id
        );


CREATE TABLE IF NOT EXISTS users.roles
(
    "id"   BIGSERIAL,
    name varchar(100) NOT NULL,
    CONSTRAINT PK_roles PRIMARY KEY ( "id" )
    );

CREATE INDEX idx_roles_name_unique ON users.roles
    (
     name
        );

CREATE TABLE IF NOT EXISTS users.permissions
(
    "id"   BIGSERIAL,
    name varchar(100) NOT NULL,
    CONSTRAINT PK_permissions PRIMARY KEY ( "id" )
    );

CREATE TABLE IF NOT EXISTS users.role_permissions
(
    permissions_id bigint NOT NULL,
    role_id        bigint NOT NULL,
    CONSTRAINT fk_permissions_role_permissions FOREIGN KEY ( permissions_id ) REFERENCES users.permissions ( "id" ),
    CONSTRAINT fk_roles_role_permissions FOREIGN KEY ( role_id ) REFERENCES users.roles ( "id" )
    );

CREATE INDEX fkIdx_130 ON users.role_permissions
    (
     permissions_id
        );

CREATE INDEX fkIdx_133 ON users.role_permissions
    (
     role_id
        );

CREATE TABLE IF NOT EXISTS users.user_roles
(
    user_id bigint NOT NULL,
    role_id bigint NOT NULL,
    CONSTRAINT fk_users_user_roles FOREIGN KEY ( user_id ) REFERENCES users.users ( "id" ),
    CONSTRAINT fk_roles_user_roles FOREIGN KEY ( role_id ) REFERENCES users.roles ( "id" )
    );

CREATE INDEX fkIdx_137 ON users.user_roles
    (
     user_id
        );

CREATE INDEX fkIdx_142 ON users.user_roles
    (
     role_id
        );
