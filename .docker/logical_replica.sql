-- On master
create publication example for table tablename;

-- On replica
create subscription example connection 'host=172.22.0.2 port=5432 dbname=logical_replica' PUBLICATION example;