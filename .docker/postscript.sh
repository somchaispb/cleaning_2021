rm -rf /var/lib/postgresql/data/*;
pg_basebackup -D /var/lib/postgresql/data -X stream -P -U replicator -Fp -R --slot replication_slot_slave1 -h master -p 5432;