BEGIN;
CREATE USER docker;

-- create database replica_test;

CREATE USER replicator WITH REPLICATION ENCRYPTED PASSWORD 'postgres' LOGIN;

-- GRANT ALL PRIVILEGES ON DATABASE replica_test TO docker;

SELECT * FROM pg_create_physical_replication_slot('replication_slot_slave1');
COMMIT;